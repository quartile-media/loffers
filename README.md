### Loffers

Loffers is a cross plateform offer discovery application which uses a user's current location and based on category preference shows the offers sorted by nearest available. The application has two modes 
- one for business owners where they can create branches, offers and 
- other for users where they can discover the offers.

![](https://sklative-main.s3.us-west-2.amazonaws.com/icon.png)