﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Xaminals.Views.Offers
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Offer : ContentPage
    {
        public Offer()
        {
            InitializeComponent();
        }
    }
}