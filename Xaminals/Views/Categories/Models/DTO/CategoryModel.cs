﻿namespace Xaminals.Views.Categories.Models.DTO
{
    public class CategoryModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public bool Selected { get; set; }
    }
}
