﻿using Rg.Plugins.Popup.Pages;
using Xamarin.Forms.Xaml;

namespace Xaminals.Views.Account
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPopup : PopupPage
    {
        public LoginPopup()
        {
            InitializeComponent();
        }
    }
}