﻿using Rg.Plugins.Popup.Pages;
using Xamarin.Forms.Xaml;

namespace Loffers.Views.Account
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ConfirmPopup : PopupPage
    {
        public ConfirmPopup()
        {
            InitializeComponent();
        }
    }
}